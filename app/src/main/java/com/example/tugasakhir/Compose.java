package com.example.tugasakhir;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

public class Compose extends AppCompatActivity {

    ImageButton uploadAttachmentBT;
    ImageButton backBT;
    EditText editTitle;
    ImageButton submitBT;
    EditText editTextIsi;
    ImageView imgView;
    ProgressBar progressBar;
    private Uri imageUri;
    final private DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("Berita");
    final private StorageReference storageReference = FirebaseStorage.getInstance().getReference();

    private Berita berita;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compose);

        uploadAttachmentBT = findViewById(R.id.btnAttacment);
        backBT = findViewById(R.id.btnBack);
        editTitle = findViewById(R.id.editTitle);
        submitBT = findViewById(R.id.btnNext);
        editTextIsi = findViewById(R.id.editTextIsi);
        imgView = findViewById(R.id.composeIMG);
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.INVISIBLE);

        if(getIntent() != null){
            Intent intent = getIntent();
            Berita berita = (Berita) intent.getSerializableExtra("berita");
            if(berita != null){
                editTitle.setText(berita.getJudul());
                editTextIsi.setText(berita.getDeskripsi());
                Glide.with(this).load(berita.getImageURL()).into(imgView);
                this.berita = berita;
            }
        }

        ActivityResultLauncher<Intent> activityResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        if (result.getResultCode() == Activity.RESULT_OK) {
                            Intent data = result.getData();
                            imageUri = data.getData();
                            // Set the selected image to your ImageButton (uploadAttachmentBT)
                            imgView.setImageURI(imageUri);
                        } else {
                            Toast.makeText(Compose.this, "No Image Selected", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
        );

        uploadAttachmentBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent photoPicker = new Intent();
                photoPicker.setAction(Intent.ACTION_GET_CONTENT);
                photoPicker.setType("image/*");
                activityResultLauncher.launch(photoPicker);
            }
        });

        submitBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(imgView != null){
                    uploadToFirebase(imageUri);
                } else {
                    Toast.makeText(Compose.this, "Please select image", Toast.LENGTH_SHORT).show();
                }
            }
        });

        backBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Compose.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private void uploadToFirebase(Uri uri){
        String judul = editTitle.getText().toString();
        String deskripsi = editTextIsi.getText().toString(); // Fixed variable name
        final StorageReference imageReference = storageReference.child(System.currentTimeMillis() + "." + getFileExtension(uri));

        imageReference.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                imageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        if(berita != null){
                            berita.setImageURL(uri.toString());
                            berita.setDeskripsi(deskripsi);
                            berita.setJudul(judul);
                            databaseReference.child(berita.getId()).setValue(berita);
                        } else {
                            berita = new Berita();
                            String key = databaseReference.push().getKey();
                            berita.setId(key);
                            berita.setJudul(judul);
                            berita.setDeskripsi(deskripsi);
                            berita.setImageURL(uri.toString());
                            databaseReference.child(key).setValue(berita);
                        }
                        progressBar.setVisibility(View.INVISIBLE);
                        Toast.makeText(Compose.this, "Uploaded", Toast.LENGTH_SHORT).show(); // Fixed activity reference
                        Intent intent = new Intent(Compose.this, MainActivity.class); // Fixed activity reference
                        startActivity(intent);
                        finish();
                    }
                });
            }
        }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(@NonNull UploadTask.TaskSnapshot snapshot) {
                progressBar.setVisibility(View.VISIBLE);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                progressBar.setVisibility(View.INVISIBLE);
                Toast.makeText(Compose.this, "Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private String getFileExtension(Uri fileUri){
        ContentResolver contentResolver = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(contentResolver.getType(fileUri));
    }

}
