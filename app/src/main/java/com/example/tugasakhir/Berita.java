package com.example.tugasakhir;

import androidx.annotation.Nullable;

import java.io.Serializable;

public class Berita implements Serializable {
    public String id;
    public String imageURL, judul, deskripsi;

    public Berita(){

    }

    public String getId() { return id; }

    public void setId(String id) { this.id = id; }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public Berita(String id, String imageURL, String judul, String deskripsi){
        this.id = id;
        this.judul = judul;
        this.deskripsi = deskripsi;
        this.imageURL = imageURL;
    }

    @Override
    public String toString(){
        return String.format("id = %s, imageUrl = %s, judul = %s, deskripsi = %s\n", id, imageURL, judul, deskripsi);
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if(obj instanceof Berita){
            Berita berita = (Berita) obj;
            return
                    berita.getId().equals(this.getId())
                    &&
                            berita.getDeskripsi().equals(this.getDeskripsi())
                    &&
                            berita.getImageURL().equals(this.getImageURL())
                    &&
                            berita.getJudul().equals(this.judul);
        } else {
            return false;
        }
    }
}
