package com.example.tugasakhir;

import static android.content.ContentValues.TAG;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class Registration extends AppCompatActivity {
    EditText editTextFullName, editTextEmail, editTextPhoneNumber, editTextPassword, editTextConfPassword;
    Button buttonReg;
    FirebaseAuth mAuth;
    ProgressBar progressBar;
    TextView textView;
    final private DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("Users");

    SharedPreferences preferences;
    boolean passVisible;

    @Override
    public void onStart() {
        super.onStart();
        Log.d("Registration", "currentStatus = " + UserStatus.status.name());
        preferences = getSharedPreferences("status", MODE_PRIVATE);
        String status = preferences.getString("status_login", "");
        if(status.equals(Status.LOGIN.name())){
            Intent intent = new Intent(getApplicationContext(), (MainActivity.class));
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        mAuth = FirebaseAuth.getInstance();
        editTextFullName = findViewById(R.id.editFullName);
        editTextEmail = findViewById(R.id.editEmail);
        editTextPhoneNumber = findViewById(R.id.editPhone);
        editTextPassword = findViewById(R.id.editPass);
        editTextConfPassword = findViewById(R.id.editConfPass);
        buttonReg = findViewById(R.id.buttonConfirmPass);
        progressBar = findViewById(R.id.progressBar);
        textView = findViewById(R.id.login);
        textView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent intent = new Intent(getApplicationContext(), Login.class);
                startActivity(intent);
            }
        });

        buttonReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Registration", "Sebelum Visible Progress Bar");
                progressBar.setVisibility(View.VISIBLE);
                Log.d("Registration", "Sesudah Visible Progress Bar");
                String email, password, fullName, phoneNumber;
                email = String.valueOf(editTextEmail.getText());
                password = String.valueOf(editTextPassword.getText());
                fullName = String.valueOf(editTextFullName.getText());
                phoneNumber = String.valueOf(editTextPhoneNumber.getText());

                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(Registration.this, "Enter email", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(password)) {
                    Toast.makeText(Registration.this, "Enter password", Toast.LENGTH_SHORT).show();
                    return;
                }

                mAuth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                Log.d("Registration", "Sebelum Gone Progress Bar");
                                progressBar.setVisibility(View.GONE);
                                if (task.isSuccessful()) {
                                    String Users = mAuth.getCurrentUser().getUid();
                                    DatabaseReference userRef = databaseReference.child(Users);

                                    Map<String, Object> user = new HashMap<>();
                                    user.put("Full Name", fullName);
                                    user.put("Email", email);
                                    user.put("Password", password);
                                    user.put("Phone Number", phoneNumber);

                                    userRef.setValue(user);

                                    Toast.makeText(Registration.this, "Account created.", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(getApplicationContext(), Login.class);
                                    Log.d("Registration", "finished");
                                    preferences = getSharedPreferences("status", MODE_PRIVATE);
                                    SharedPreferences.Editor editor = preferences.edit();
                                    editor.putString("status_login", Status.REGISTER.name());
                                    editor.apply();
                                    startActivity(intent);
                                } else {
                                    Toast.makeText(Registration.this, "Authentication failed.", Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                        .addOnFailureListener(command -> {
                            Log.e("Registration", "error occurred with message " + command.getMessage());
                        });
            }
        });
        editTextPassword.setOnTouchListener(new View.OnTouchListener(){
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int Right=2;
                if(event.getAction()==MotionEvent.ACTION_UP){
                    if (event.getRawX() >= editTextPassword.getRight() - editTextPassword.getCompoundDrawables()[Right].getBounds().width()){
                        int selection = editTextPassword.getSelectionEnd();
                        if(passVisible){
                            //set drawable image
                            editTextPassword.setCompoundDrawablesRelativeWithIntrinsicBounds(0,0,R.drawable.pass_hide,0);
                            //for hide password
                            editTextPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                            passVisible=false;

                        }else{
                            //set drawable image
                            editTextPassword.setCompoundDrawablesRelativeWithIntrinsicBounds(0,0,R.drawable.password,0);
                            //for hide password
                            editTextPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                            passVisible=true;

                        }
                        editTextPassword.setSelection(selection);
                        return true;
                    }
                }
                return false;
            }
        });
        editTextConfPassword.setOnTouchListener(new View.OnTouchListener(){
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int Right=2;
                if(event.getAction()==MotionEvent.ACTION_UP){
                    if (event.getRawX() >= editTextConfPassword.getRight() - editTextConfPassword.getCompoundDrawables()[Right].getBounds().width()){
                        int selection = editTextConfPassword.getSelectionEnd();
                        if(passVisible){
                            //set drawable image
                            editTextConfPassword.setCompoundDrawablesRelativeWithIntrinsicBounds(0,0,R.drawable.pass_hide,0);
                            //for hide password
                            editTextConfPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                            passVisible=false;

                        }else{
                            //set drawable image
                            editTextConfPassword.setCompoundDrawablesRelativeWithIntrinsicBounds(0,0,R.drawable.confirm_password,0);
                            //for hide password
                            editTextConfPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                            passVisible=true;

                        }
                        editTextConfPassword.setSelection(selection);
                        return true;
                    }
                }
                return false;
            }
        });
    }
}
