package com.example.tugasakhir;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import com.example.tugasakhir.R;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class InputAdapter extends RecyclerView.Adapter<InputAdapter.BeritaViewHolder> {

    private ArrayList<Berita> berita;
    private Context context;
    private OnItemClickListener mListener;

    private final String TAG = "InputAdapter";

    public interface OnItemClickListener {
        void onItemClick(String judul); // Modify the parameters as needed
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public InputAdapter(ArrayList<Berita> berita, Context context) {
        this.berita = berita;
        this.context = context;
    }

    @NonNull
    @Override
    public BeritaViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.input_adapter, parent, false);
        return new BeritaViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BeritaViewHolder holder, int position) {
        Log.d(TAG, "berita on bind view holder = " + berita.get(position));
        Glide.with(context).load(berita.get(position).getImageURL()).into(holder.recycleImage);
        holder.recycleJudul.setText(berita.get(position).getJudul());

        holder.tripleDot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(context, holder.tripleDot);
                popupMenu.getMenuInflater().inflate(R.menu.icon_menu, popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        final int menuEdit = R.id.menuEdit;
                        final int menuDelete = R.id.menuDelete;
                        if (item.getItemId() == menuEdit){
                            handleEdit(context, berita.get(position));
                        } else if (item.getItemId() == menuDelete){
                            handleDelete(berita.get(position));
                        } return true;
                    }
                });
                popupMenu.show();
            }
        });
    }

    private void handleEdit(Context context, Berita berita) {
        // Implement logic to edit the data (open edit activity or dialog)
        // You may pass the 'berita' object or its ID to the edit activity or dialog
        // Update the data in the Firebase Realtime Database with the new values
        Intent intent = new Intent(context.getApplicationContext(), Compose.class);
        intent.putExtra("berita", berita);
        context.startActivity(intent);
    }

    private void handleDelete(Berita beritaData) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Confirm Deletion");
        builder.setMessage("Are you sure you want to delete this item?");
        builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Remove the data from the Firebase Realtime Database
                DatabaseReference beritaRef = FirebaseDatabase.getInstance().getReference("Berita").child(beritaData.getId());
                beritaRef.removeValue(); // Remove the item by ID

//                int changedIndex = -1;

                for (int i = 0 ; i < berita.size() ; i++){
                    if(berita.get(i).getId().equals(beritaData.getId())){
                        Log.d(TAG, "removed berita = " + berita.get(i));
                        berita.remove(i);
                        Log.d(TAG, "size = " + berita.size());
//                        changedIndex = i;
                        break;
                    }
                }

                // Notify the adapter that the data set has changed
                notifyDataSetChanged();

                Toast.makeText(context, "Item deleted", Toast.LENGTH_SHORT).show();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Cancel the deletion
                dialog.dismiss();
            }
        });

        builder.create().show();
    }

    @Override
    public int getItemCount() {
        return berita.size();
    }

    public class BeritaViewHolder extends RecyclerView.ViewHolder {
        ImageView recycleImage;
        TextView recycleJudul;
        ImageButton tripleDot;

        public BeritaViewHolder(@NonNull View itemView) {
            super(itemView);

            recycleImage = itemView.findViewById(R.id.imageView);
            recycleJudul = itemView.findViewById(R.id.tvJudul);
            tripleDot = itemView.findViewById(R.id.buttonPopUp);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    Log.d(TAG, "position = " + position);
                    Log.d(TAG, "berita = " + berita.get(position));
                    Log.d(TAG, "mListenber = " + mListener);
                    // Your existing logic for starting NewsClicked activity
                    Intent intent = new Intent(context, NewsClicked.class);
                    intent.putExtra("berita", berita.get(position));
                    context.startActivity(intent);
                }
            });
        }
    }
}
